import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
train = pd.read_csv('C:/Users/hp/Downloads/Salary.csv')
test = pd.read_csv('C:/Users/hp/Downloads/Salary.csv')
x_yeaexp = train['YearsExperience']
y_sal = train['Salary']
x_test = train['YearsExperience']
y_test = train['Salary'] + 0.8
x_train_yeaexp = np.array(x_yeaexp)
y_train_sal = np.array(y_sal)
x_test = np.array(x_test)
y_test = np.array(y_test)
#x_train_yeaexp = x_train_yeaexp.reshape(-1,1)
#x_test = x_test.reshape(-1,1)
#print(x_train_yeaexp.shape)
#x_train_yeaexp=x_train_yeaexp.reshape(30,1)
n = 30
lr = 0.0001
c = 0
m = 0
epochs = 0
# Applying SGD for Training Data Sets
while epochs < 1000:
    y = c + (m * x_train_yeaexp)
    error = y - y_train_sal
    m_s_e = np.sum(error**2)
    m_s_e = m_s_e/n
    c = c - lr*2*np.sum(error)/n
    m = m - lr*2*np.sum(error*x_train_yeaexp)/n
    epochs += 1 
#x_test = x_test.reshape(30,1)
#y_test = y_test.reshape(30,1)
#a0 = c[0]
#a1 = m[0]
#y_pred = a0 + a1*x_train_yeaexp
'''y_plot = []
for i in range(100):
    y_plot.append(a0 + a1 * i)
#y_plot = np.asarray(y_plot)
y#_plot = y_plot.reshape(100,30)
'''
#plt.figure(figsize=(10,10))
plt.scatter(x_test,y_test,color='red',label='test_data')
#x = np.linspace(max_x,min_x,100,30)
plt.plot(x_train_yeaexp,y,color='black',label = 'Trained best Fit')
#plt.plot(range(len(y_plot)),y_plot,color='black',label= 'pred')
plt.legend()
plt.show()